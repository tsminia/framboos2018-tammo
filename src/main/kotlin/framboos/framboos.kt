package framboos

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.InputStreamReader
import java.io.BufferedReader
import java.io.PrintWriter
import java.net.Socket
import java.util.*
import kotlin.math.absoluteValue

val test = false
val gridSize = 40

class GreetClient(ip: String, port: Int) {
    private var clientSocket: Socket = Socket(ip, port)
    private var out: PrintWriter = PrintWriter(clientSocket.getOutputStream(), true)
    private var input: BufferedReader = BufferedReader(InputStreamReader(clientSocket.getInputStream()))

    fun sendMessage(msg: String) {
        out.println(msg)
//        return input.readLine()
    }

    fun readLine(): String {
        return input.readLine()
    }

    fun stopConnection() {
        input.close()
        out.close()
        clientSocket.close()
    }
}

class Location(val x: Int, val y: Int)
class User(val name: String, val email: String, val location: Location, val points: Int, val raspberries: Int, val respawn: Int)
class Welcome(val type: String, val user: User)

class BotUpdate(val name: String, val location: Location)
class Player(val name: String, val points: Int, val location: Location)
class Update(val type: String, val tick: Int, val level: Int, val raspberries: Array<Location>, val bots: Array<BotUpdate>, val user: User, val otherPlayers: Array<Player>)

class Move(val type: String, val tick: Int, val move: String)

fun main(args: Array<String>) {
    val mapper = ObjectMapper().registerModule(KotlinModule())

    while (true) {
        try {
            val client = GreetClient("192.168.3.118", 3333)
            while (true) {
                val lineIn = client.readLine()
                if (test) println(lineIn)

                try {
                    val update = mapper.readValue<Update>(lineIn)
//                    val update = Klaxon().parse<Update>(lineIn)
                    if (update != null) {
//                        val rmove = determineMove(update!!)
//                        val rmoveString = """{"type":"move","tick":${update.tick},"move":"${rmove}"}"""
//                        client.sendMessage(rmoveString)
                        var move = determineMove(update!!)
                        val moveString = """{"type":"move","tick":${update.tick},"move":"${move}"}"""
                        if (test) println(moveString)
                        client.sendMessage(moveString)
                    }
                } catch (e: Exception) {
                    try {
//                        val welcome = Klaxon().parse<Welcome>(lineIn)
                        val welcome = mapper.readValue<Welcome>(lineIn)
                        if (welcome != null && welcome.type == "welcome") {
                            val setName = if (test) {
                                """{ "type":"set_name", "name":"tammo-test", "email":"tammosminia@gmail.com" }"""
                            } else {
                                """{ "type":"set_name", "name":"Tammo", "email":"tammo.sminia@jdriven.com" }"""
                            }
                            if (test) println(setName)
                            client.sendMessage(setName)
                        }
                    } catch (e: Exception) {
                        //ignore
                    }

                }

            }
        } catch (e: Exception) {
            println("client crashed")
        }
    }
}

fun distance(from: Int, to: Int): Int {
    val d = (from - to).absoluteValue
    if (d <= 20) {
        return d
    } else {
        return 40 - d
    }
}
fun distance(from: Location, to: Location): Int {
    return distance(from.x, to.x) + distance(from.y, to.y)
}

fun closest(self: Location, locations: Array<Location>): Location? {
    return locations.minBy { l -> distance(self, l) }
}

fun directionTo(from: Location, to: Location): String {
    val horizontalSignedDistance = to.x - from.x
    val verticalSignedDistance = to.y - from.y
    if (horizontalSignedDistance > 0 && horizontalSignedDistance < 20 || horizontalSignedDistance < 0 && horizontalSignedDistance < -20) {
        return "RIGHT"
    } else if (horizontalSignedDistance < 0 && horizontalSignedDistance > -20 || horizontalSignedDistance > 0 && horizontalSignedDistance > 20) {
        return "LEFT"
    } else if (verticalSignedDistance > 0 && verticalSignedDistance < 20 || verticalSignedDistance < 0 && verticalSignedDistance < -20) {
        return "DOWN"
    } else if (verticalSignedDistance < 0 && verticalSignedDistance > -20 || verticalSignedDistance > 0 && verticalSignedDistance > 20) {
        return "UP"
    } else {
        return "STAY"
    }
}

fun inverseDirection(d: String): String {
    return when (d) {
        "RIGHT" -> "LEFT"
        "LEFT" -> "RIGHT"
        "UP" -> "DOWN"
        "DOWN" -> "UP"
        else -> "STAY"
    }
}

fun determineMove(update: Update): String {
    val closestGhost = closest(update.user.location, update.bots.map { b -> b.location }.toTypedArray())
    if (closestGhost != null && distance(update.user.location, closestGhost) < 3) {
        val direction = directionTo(update.user.location, closestGhost)
        return inverseDirection(direction)
    } else {
        val closestRaspberry = closest(update.user.location, update.raspberries)
        if (closestRaspberry != null) {
            val direction = directionTo(update.user.location, closestRaspberry)
            return direction
        }
    }
    return randomMove(update)
}

fun modulo(x: Int): Int {
    if (x >= 40) {
        return x - 40
    } else if (x < 0) {
        return x + 40
    } else {
        return x
    }
}

fun newLocation(l: Location, move: String): Location {
    return when (move) {
        "RIGHT" -> Location(modulo(l.x + 1), l.y)
        "LEFT" -> Location(modulo(l.x - 1), l.y)
        "UP" -> Location(l.x, modulo(l.y - 1))
        "DOWN" -> Location(l.x, modulo(l.y + 1))
        else -> Location(l.x, l.y)
    }
}

fun randomMove(update: Update): String {
    val moves = listOf("UP", "DOWN", "LEFT", "RIGHT")
    return moves.get(Random().nextInt(4))
}


